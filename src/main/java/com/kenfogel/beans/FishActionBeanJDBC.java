package com.kenfogel.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A magic bean that returns a list of fish
 *
 * @author Ken
 *
 */
@Named
@RequestScoped
public class FishActionBeanJDBC implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(FishActionBeanJDBC.class);

    // See the context.xml for the datasource
    @Resource(lookup ="java:app/jdbc/myAquarium")
    private DataSource ds;

    public List<FishDataJDBC> getAll() throws SQLException {

        if (ds == null) {
            throw new SQLException("Can't get data source");
        }

        List<FishDataJDBC> fishies = new ArrayList<>();

        try (Connection conn = ds.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet resultSet = stmt.executeQuery("SELECT * FROM Fish");) {
            while (resultSet.next()) {
                FishDataJDBC fd = new FishDataJDBC();
                fd.setCommonName(resultSet.getString("COMMONNAME"));
                fd.setDiet(resultSet.getString("DIET"));
                fd.setKh(resultSet.getString("KH"));
                fd.setLatin(resultSet.getString("LATIN"));
                fd.setPh(resultSet.getString("PH"));
                fd.setFishSize(resultSet.getString("FISHSIZE"));
                fd.setSpeciesOrigin(resultSet.getString("SPECIESORIGIN"));
                fd.setStocking(resultSet.getString("STOCKING"));
                fd.setTankSize(resultSet.getString("TANKSIZE"));
                fd.setTemp(resultSet.getString("TEMP"));
                fd.setId(resultSet.getInt("ID"));

                fishies.add(fd);
            }
        }
        return fishies;
    }
}
