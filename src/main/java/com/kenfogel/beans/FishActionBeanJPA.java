package com.kenfogel.beans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A magic bean that returns a list of fish
 *
 * @author Ken
 *
 */
@Named
@RequestScoped
public class FishActionBeanJPA implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(FishActionBeanJPA.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    public List<FishDataJPA> getAll() throws SQLException {

        // Criteria for Select all records
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<FishDataJPA> cq = cb.createQuery(FishDataJPA.class);
        Root<FishDataJPA> fish = cq.from(FishDataJPA.class);
        cq.select(fish);
        TypedQuery<FishDataJPA> query = entityManager.createQuery(cq);
        List<FishDataJPA> fishies = query.getResultList();

        return fishies;
    }
}
